#include <iostream>
#include "rollup.hpp"

class Minimum: public Rollup
{
public:
	Minimum():
		Rollup(),
		isFirst(true),
		min(0.0)
	{
	}

	virtual ~Minimum()
	{
	}

	void update(double value) {
		if (isFirst) {
			min = value;
			isFirst = false;
			return;
		}
		min = std::min(min, value);
	}

	double getValue()
	{
		return min;
	}

private:
	bool isFirst;
	double min;
};

int main(const int argc, char *argv[]) {
	rollupMain<Minimum>(argc, argv);
}
