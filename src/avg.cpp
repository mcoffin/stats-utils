#include <iostream>

int main(const int argc, const char *argv[])
{
	double sum = 0;
	long count = 0;
	double v;
	while (std::cin >> v) {
		sum += v;
		count++;
	}
	std::cout << v << std::endl;
	return 0;
}
