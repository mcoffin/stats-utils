#include <iostream>
#include "rollup.hpp"
#include <unistd.h>

double Rollup::run(int argc, char *argv[])
{
	bool printValues = false;
	int opt;
	opterr = 0;
	optind = 0;
	while ((opt = getopt(argc, argv, "p")) != -1) {
		switch (opt) {
			case 'p':
				printValues = true;
				break;
			case '?':
				std::cerr << "Invalid argument: -" << optopt << std::endl;
				exit(1);
				break;
			default:
				abort();
		}
	}
	double v;
	while (std::cin >> v) {
		if (printValues) {
			std::cout << v << std::endl;
		}
		update(v);
	}
	return getValue();
}
