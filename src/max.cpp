#include <iostream>
#include "rollup.hpp"

class Maximum: public Rollup
{
public:
	Maximum():
		Rollup(),
		isFirst(true),
		max(0.0)
	{
	}

	virtual ~Maximum()
	{
	}

	void update(double value) {
		if (isFirst) {
			max = value;
			isFirst = false;
			return;
		}
		max = std::max(max, value);
	}

	double getValue()
	{
		return max;
	}
private:
	bool isFirst;
	double max;
};

int main(const int argc, char *argv[]) {
	rollupMain<Maximum>(argc, argv);
}
