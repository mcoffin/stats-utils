#pragma once

class Rollup {
public:
	virtual ~Rollup() {};
	virtual void update(double value) = 0;
	virtual double getValue() = 0;
	double run(int argc, char *argv[]);
};

template<class T>
void rollupMain(const int argc, char *argv[]) {
	T rollup;
	std::cout << rollup.run(argc, argv) << std::endl;
}
